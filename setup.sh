export ROOT_SOURCE="/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.06.06-a2c9d/x86_64-slc6-gcc49-opt/bin/thisroot.sh";
#export HEXPLOT_DIR="/home/software/HGCAL_sensor_analysis/bin";
export HEXPLOT_DIR="/home/tquast/HGCAL_sensor_analysis/bin";
export WORKFLOW_DIR="/afs/cern.ch/user/t/tquast/TCT_analysis_workflow_dev";
export DB_DIR="$WORKFLOW_DIR/database/csv";
export EMAP_DIR="$WORKFLOW_DIR/database/emaps";

export PYTHONPATH=$WORKFLOW_DIR:$PYTHONPATH;

export TMPFILES_DIR_TCT="/home/tquast/tct_Spring2021"
source $WORKFLOW_DIR/workflow/setup.sh;