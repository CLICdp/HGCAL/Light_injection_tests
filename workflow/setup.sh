DeleteAnalysisFiles_TCT() {
	MEASUREMENTID=$1
	python $WORKFLOW_DIR/workflow/tasks/purge_output.py --MeasurementID $MEASUREMENTID --TaskName AnalyseAverageWaveform --Recursive 1;
}

AnalyseAllMeasurements_TCT() {
	luigi --module workflow.tasks.wrappers TCT.AnalyseAllMeasurements --workers $1 --local-scheduler;
}

AnalyseMeasurement_TCT() {
	MEASUREMENTID=$2
	luigi --module workflow.tasks.wrappers TCT.AnalyseMeasurement --workers $1 --MeasurementID $MEASUREMENTID --local-scheduler;
}

MakeCellFiguresForAllCells_TCT() {
	MEASUREMENTID=$2
	luigi --module workflow.tasks.vis TCT.MakeCellFiguresForAllCells --workers $1 --MeasurementID $MEASUREMENTID --local-scheduler;
}

MakeCellFigures_TCT() {
	MEASUREMENTID=$2
	CELL=$3
	luigi --module workflow.tasks.vis TCT.MakeCellFigures --workers $1 --MeasurementID $MEASUREMENTID --CellNr $CELL --local-scheduler;	
}

MakeHexPlotsForAllBiasVoltages_TCT() {
	MEASUREMENTID=$2
	luigi --module workflow.tasks.vis TCT.MakeHexPlotsForAllBiasVoltages --workers $1 --MeasurementID $MEASUREMENTID --local-scheduler;
}

MakeHexPlotForBiasVoltage_TCT() {
	MEASUREMENTID=$2
	BIASVOLTAGE=$3
	luigi --module workflow.tasks.vis TCT.MakeHexPlotForBiasVoltage --workers $1 --MeasurementID $MEASUREMENTID --BiasVoltage $BIASVOLTAGE --local-scheduler;	
}

MakeHexPlotChannelMap_TCT() {
	MEASUREMENTID=$2
	luigi --module workflow.tasks.vis TCT.MakeHexPlotChannelMap --workers $1 --MeasurementID $MEASUREMENTID --local-scheduler;
}

SignalVsVoltage_TCT() {
	MEASUREMENTID=$2
	luigi --module workflow.tasks.vis TCT.SignalVsVoltage --workers $1 --MeasurementID $MEASUREMENTID --local-scheduler;
}

AnalyseAverageWaveform_TCT() {
	MEASUREMENTID=$2
	luigi --module workflow.tasks.ana TCT.AnalyseAverageWaveform --workers $1 --MeasurementID $MEASUREMENTID --local-scheduler;
}
