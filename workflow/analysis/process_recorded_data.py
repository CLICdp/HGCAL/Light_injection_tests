# Author: Thorben Quast, thorben.quast@cern.ch
# Created: 24 Feb 2021
# Last modified: 24 Feb 2021
# Description:
#

from common.utility.source import source
from tqdm import tqdm
import os
import sys
import subprocess
import argparse
from common.utility import fs

parser = argparse.ArgumentParser()
parser.add_argument("--infilePath", type=str,
                    help="(input) Path to the raw csv file directory.", default="", required=True)
parser.add_argument("--outfilePath", type=str, help="(output) Directory to which the processed data are to be written.",
                    default="", required=True)
args = parser.parse_args()

outfileDir = fs.create_dir(os.path.abspath(args.outfilePath))
infileDir = args.infilePath
scriptName = os.path.join(os.path.dirname(
    os.path.abspath(__file__)), "fast_process_data.py")

csv_files = [_file for _file in os.listdir(infileDir) if ".csv" in _file]

_cmds = []
for _csvfile in csv_files:
    infilePath = os.path.join(infileDir, _csvfile)
    outfileName = _csvfile.split("V_")[0]+"V"

    outfilePath = os.path.join(outfileDir, outfileName+"_analysed.csv")
    outfilePathReduced = os.path.join(outfileDir, outfileName+"_reduced.csv")

    cmd = "python %s " % scriptName
    cmd += "--infilePath %s " % infilePath
    cmd += "--dataFormat 1 "
    cmd += "--invertSiliconSignal 1 "
    cmd += "--keepFrac 2 "
    cmd += "--deleteRaw 0 "
    cmd += "--outfilePath %s " % outfilePath
    cmd += "--outfilePathReduced %s " % outfilePathReduced
    _cmds.append(cmd)


# execute the commands
env = {}
env.update(os.environ)
env.update(source("/afs/cern.ch/user/t/tquast/TCT_analysis_workflow_dev/setup.sh"))

for _cmd in tqdm(_cmds, unit="files"):
    print _cmd
    p = subprocess.Popen(_cmd, stdout=sys.stdout,
                         stderr=sys.stderr, shell=True, env=env)
    out, err = p.communicate()
    code = p.returncode
    if code != 0:
        raise Exception("Subprocess execution failed")
