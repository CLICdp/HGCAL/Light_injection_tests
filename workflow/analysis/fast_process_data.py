# Author: Thorben Quast, thorben.quast@cern.ch
# Created: 19 Feb 2021
# Last modified: 22 Feb 2021
# Description: 
# 1. Assigns readout and sample indices
# 2. Computes & saves average, median, rms, and sem per sample, output file = dictionary of pandas dataframe with pickle
# 3. Visualises average and rms waveforms
# 4. Reduces data content and saves a fraction of the data, output file = pandas dataframe with pickle
# 5. Visualises samples waveforms

import os, time, argparse
import numpy as np
import pandas as pd
from tqdm import tqdm
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
start_time = time.time()


def log(msg=None, ref_time=start_time):
    time_msg = "[%.1fs]" % (time.time() - start_time)
    if msg == None:
        print(time_msg)
    else:
        print(time_msg+" "+str(msg))


def plot_analysed_waveform(df, label, outpath):
    plt.clf()
    fig, ax = plt.subplots(3, figsize=(8, 10))
    for index, quantity in enumerate(["raw", "amplified", "laser"]):
        # draw the average waveform
        ax[index].plot(df.index, df[quantity],                              #index = sample index
                       color=["black", "blue", "red"][index], label=label, linewidth=3.0)
        if index == 2:
            ax[index].set_xlabel("Time [ns]")
        ax[index].set_ylabel("%s signal [mV]" % label)
        ax[index].set_title(quantity)
        ax[index].legend()
    fig.savefig(outpath)


def plot_sample_waveforms(df, label, outpath):
    plt.clf()
    fig, ax = plt.subplots(3, figsize=(8, 10))
    for index, quantity in enumerate(["raw", "amplified", "laser"]):
        # draw the average waveform
        for df_rdout in df.groupby(df['readout_index']):
            ax[index].plot(df_rdout[1]['sample_index'], df_rdout[1][quantity],
                           label="rdout:" + str(df_rdout[0]), linewidth=1.0)
        if index == 2:
            ax[index].set_xlabel("Time [ns]")
        ax[index].set_ylabel("%s signal [mV]" % label)
        ax[index].set_title(quantity)
        ax[index].legend()
    fig.savefig(outpath)


parser = argparse.ArgumentParser()
parser.add_argument("--infilePath", type=str,
                    help="(input) Path to the raw csv file.", default="Waveform_cell_78_-275V_20210212_224152.csv", required=False)
parser.add_argument("--dataFormat", type=int,
                    help="(option) Format of the input data", default=1, required=False)
parser.add_argument("--invertSiliconSignal", type=int,
                    help="(option) Signal waveform from silicon to be inverted?", default=1, required=False)
parser.add_argument("--keepFrac", type=int,
                    help="(option) Fraction of the input data to store for potential subsequent cross-checks", default=1, required=False)
parser.add_argument("--deleteRaw", type=int,
                    help="(optional) Delete raw input file (1=yes, 0=no)", default=0, required=False)
parser.add_argument("--outfilePath", type=str, help="(output) Path to the file containing the average waverform and sample RMS.",
                    default="Waveform_cell_78_-275V_20210212_224152_analysed.csv", required=False)
parser.add_argument("--outfilePathReduced", type=str,
                    help="(output) Path to the h5-converted file containing a fraction of the raw data.", default="Waveform_cell_78_-275V_20210212_224152_reduced.csv", required=False)
args = parser.parse_args()

# export as option: argparse
DATAFORMAT = args.dataFormat


infilePath = os.path.abspath(args.infilePath)
outfilePath = os.path.abspath(args.outfilePath)
outfilePathReduced = os.path.abspath(args.outfilePathReduced)
NHEADERLINES = -1

if DATAFORMAT == 1:
    NHEADERLINES = 12
    log("Reading the data from %s" % infilePath)
    data = np.genfromtxt(infilePath, skip_header=NHEADERLINES, usecols=(0, 1, 2, 3),
                         unpack=True, delimiter=",").transpose()
    data = np.column_stack((data, np.zeros(len(data))))
    data = np.column_stack((data, np.zeros(len(data))))

    log("Assigning the sample index given the timestamp")
    # assign indexes for samples and readouts
    t = data[0][0]
    sample_index = 0
    readout_number = 1
    for i in tqdm(range(len(data))):
        if t <= data[i][0]:
            sample_index += 1
        else:
            readout_number += 1
            sample_index = 1
        t = data[i][0]
        data[i][4] = readout_number
        data[i][5] = sample_index

    log("Convert data to pandas dataframe")
    data_pd = pd.DataFrame(data, columns=[
                           "time", "raw", "amplified", "laser", "readout_index", "sample_index"])

    log("Rejection of infinity readings")
    # reject infinities:
    data_pd = data_pd.replace([np.inf, -np.inf], np.nan)
    data_pd = data_pd.dropna()

    # conversion to mV
    log("Conversion from V to mV")
    data_pd.raw = data_pd.raw.astype("float16")*1000.
    data_pd.amplified = data_pd.amplified.astype("float16")*1000.
    data_pd.laser = data_pd.laser.astype("float16")*1000.

    if bool(args.invertSiliconSignal):
        log("Inversion of signal waveform in the silicon")
        data_pd.raw = -data_pd.raw
        data_pd.amplified = -data_pd.amplified

    log("Conversion from int32 to unint16 for sample and readout indices")
    data_pd.readout_index = data_pd.readout_index.astype("uint16")
    data_pd.sample_index = data_pd.sample_index.astype("uint16")

    # reshuffle columns
    log("Ordering of columns")
    data_pd = data_pd.drop(columns="time")
    data_pd = data_pd[["readout_index",
                       "sample_index", "raw", "amplified", "laser"]]

    # compute average, median, rms, standard error of waveforms
    log("Computing waveform: average")
    # average
    avg_waveform_df = data_pd.groupby("sample_index").mean()
    avg_waveform_df = avg_waveform_df.drop(columns=["readout_index"])
    avg_waveform_df = avg_waveform_df.assign(quantity=0)
    # median
    log("Computing waveform: median")
    median_waveform_df = data_pd.groupby("sample_index").median()
    median_waveform_df = median_waveform_df.drop(columns=["readout_index"])
    median_waveform_df = median_waveform_df.assign(quantity=1)
    # and rms
    log("Computing waveform: rms")
    rms_waveform_df = data_pd.groupby("sample_index").std()
    rms_waveform_df = rms_waveform_df.drop(columns=["readout_index"])
    rms_waveform_df = rms_waveform_df.assign(quantity=2)
    # and standard error mean
    log("Computing waveform: standard error mean")
    sem_waveform_df = data_pd.groupby("sample_index").sem()
    sem_waveform_df = sem_waveform_df.drop(columns=["readout_index"])
    sem_waveform_df = sem_waveform_df.assign(quantity=3)

    analysed_df = pd.concat([avg_waveform_df, median_waveform_df, rms_waveform_df, sem_waveform_df])

    log("Storing analysed waveforms to %s" % outfilePath)
    with open(outfilePath, "wb") as outfile:
        analysed_df.to_csv(outfile, header=["raw", "amplified", "laser", "0_avg__1_med__2_rms__3_sem"])

    # plotting of analysed waveforms
    log("Plotting average waveform")
    plot_analysed_waveform(avg_waveform_df, "average",
                           outfilePath.replace(".csv", "_average.pdf"))
    log("Plotting rms waveform")
    plot_analysed_waveform(rms_waveform_df, "rms",
                           outfilePath.replace(".csv", "_rms.pdf"))

    # dataset reduction
    log("Select %i percent of all raw waveforms" % args.keepFrac)
    Nreadouts = data_pd['readout_index'].max()
    keepEvery = int(100/args.keepFrac)
    keepReadouts = [i for i in range(Nreadouts) if i % keepEvery == 1]
    data_reduced_pd = data_pd[data_pd['readout_index'].isin(keepReadouts)]

    log("Plotting sample waveforms")
    plot_sample_waveforms(data_reduced_pd, "raw",
                          outfilePathReduced.replace(".csv", ".pdf"))

    log("Write reduced raw data to %s" % outfilePathReduced)
    with open(outfilePathReduced, "wb") as outfile:
        data_reduced_pd.to_csv(outfile, header = ["readout_index", "sample_index", "raw", "amplified", "laser"], index=False)

else:
    raise NotImplementedError

if args.deleteRaw:
    headerFilepath = infilePath.replace(".csv", "_header.csv")
    log("Save the header information to %s" % headerFilepath)
    outfile = open(headerFilepath, "w")
    infile = open(infilePath, "r")
    for x in range(NHEADERLINES):
        outfile.write(next(infile))
    outfile.close()
    infile.close()
    
    log("Delete raw input file %s" % infilePath)
    os.remove(infilePath)
log()
