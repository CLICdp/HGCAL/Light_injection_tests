# Author: Thorben Quast, thorben.quast@cern.ch
# Created: 25 Feb 2021
# Last modified: 25 Feb 2021
# Description:
#

import argparse
import os
import numpy as np
import pandas as pd
import pickle
import re
from tqdm import tqdm
from database.database import tct_db
from workflow.utility.signal_extraction import *
from workflow.conf.analysis import OSCILLOSCOPESETTINGS


parser = argparse.ArgumentParser()
parser.add_argument("--infilePath", type=str,
                    help="(input) Path to the raw csv file directory.", default="/home/tquast/TCT_2021/1007_typeA_neg20deg", required=True)
parser.add_argument("--fileNamePattern", type=str, help="(input) Input file pattern.",
                    default="8inch_LD_1104_neg20deg_1p8Vatt_0neq_<VOLTAGE>V_cell<CELL>__analysed.csv", required=True)
parser.add_argument("--measurementID", type=str,
                    help="Indicate measurement type for labels", default="", required=True)
parser.add_argument("--averageWaveformFile", type=str,
                    help="(output) Path to which the average waveforms are written.", default="/home/tquast/TCT_2021/1007_typeA_neg20deg/average_waveforms.root", required=True)
parser.add_argument("--processedFile", type=str,
                    help="(output) Path to processed file.", default="/home/tquast/TCT_2021/1007_typeA_neg20deg/processed.csv", required=True)
parser.add_argument("--phaseSpaceFile", type=str,
                    help="(output) Path to file documenting the tested cells and voltages.", default="/home/tquast/TCT_2021/1007_typeA_neg20deg/processed.pkl", required=True)
args = parser.parse_args()

print("Loading oscilloscope settings")
_osc_setting = tct_db.get_oscilloscopeSetting(args.measurementID)
if not _osc_setting in OSCILLOSCOPESETTINGS:
    import sys
    sys.exit("Setting %s not defined." % _osc_setting)
NSSCALE = OSCILLOSCOPESETTINGS[_osc_setting]["NSSCALE"]
SIGNALRANGE_MIN = OSCILLOSCOPESETTINGS[_osc_setting]["SIGNALRANGE_MIN"]
SIGNALRANGE_MAX = OSCILLOSCOPESETTINGS[_osc_setting]["SIGNALRANGE_MAX"]
NOISERANGE_MIN = OSCILLOSCOPESETTINGS[_osc_setting]["NOISERANGE_MIN"]
NOISERANGE_MAX = OSCILLOSCOPESETTINGS[_osc_setting]["NOISERANGE_MAX"]


infileDir = args.infilePath

# compile the regular expression for identifying the filenames
reg_exp = args.fileNamePattern
reg_exp = reg_exp.replace("CELLNR", "(?P<cell>.*)")
reg_exp = reg_exp.replace("VOLTAGEVALUE", "(?P<voltage>.*)")
reg_exp = re.compile(reg_exp)

# get all csv files
all_csv_files = [_file for _file in os.listdir(infileDir) if ".csv" in _file]

# files_to_process
csv_files_to_process = []

# sort out the relevant files
print("Identifying all files to process")
for _csvfile in tqdm(all_csv_files, unit="file candidates"):
    match_exp = reg_exp.match(_csvfile)
    if match_exp == None:
        continue
    cell = int(match_exp.group("cell"))
    # convert voltages to positive values
    voltage = abs(int(match_exp.group("voltage")))

    csv_files_to_process.append(
        (cell, voltage, os.path.join(infileDir, _csvfile)))

if len(csv_files_to_process) == 0:
    import sys
    sys.exit("Nothing to process. Please check your database entries.")

print(len(csv_files_to_process), "data files to be processed")

# processing of the data files
out_data = []
# average waveform graphs still to be written out
avg_waveform_graphs = []

for CELL_NR, VOLTAGE, inputFile in tqdm(csv_files_to_process, unit=" data files"):
    # read input file
    inputData = pd.read_csv(inputFile)

    # extract mean (plus error) and rms data
    mean_data = inputData[inputData["0_avg__1_med__2_rms__3_sem"] == 0]
    mean_data = mean_data.drop(columns="0_avg__1_med__2_rms__3_sem")
    mean_err_data = inputData[inputData["0_avg__1_med__2_rms__3_sem"] == 3]
    mean_err_data = mean_err_data.drop(columns="0_avg__1_med__2_rms__3_sem")
    rms_data = inputData[inputData["0_avg__1_med__2_rms__3_sem"] == 2]
    rms_data = rms_data.drop(columns="0_avg__1_med__2_rms__3_sem")

    entry = {"raw": None, "amplified": None}

    # signal analysis
    for _type in entry:
        y = np.array(mean_data[_type])
        y_err = np.array(mean_err_data[_type])

        # waveform analysis
        # 1. Amplitude
        graph = ROOT.TGraphErrors()
        graph.SetName("average_waveform_cell%i_%iV_%s" %
                      (CELL_NR, VOLTAGE, _type))
        amplitude, amplitude_error = compute_amplitude(y, y_err, graph, NSSCALE)

        # 2. integral
        integral, integral_error = compute_integral(
            y, y_err, SIGNALRANGE_MIN, SIGNALRANGE_MAX, NSSCALE)

        # store to array
        entry[_type] = [CELL_NR, VOLTAGE, _type,
                        amplitude, amplitude_error, integral, integral_error]

        avg_waveform_graphs.append(graph)

    # noise analysis
    for _type in entry:
        y = np.array(rms_data[_type])
        y_noise = y[NOISERANGE_MIN:NOISERANGE_MAX]
        noise = np.mean(y_noise)
        noise_error = np.std(y_noise)
        # store to array
        entry[_type].append(noise)
        entry[_type].append(noise_error)

    # append to output data
    for quantity in entry:
        out_data.append(tuple(entry[quantity]))

# create pandas dataframe
df_data = pd.DataFrame(out_data, columns=[
                       "Cell", "Voltage", "Type", "Amplitude", "Amplitude_err", "Integral", "Integral_err", "Noise", "Noise_err"])
# sort by cell and bias voltage
df_data = df_data.sort_values(["Cell", "Voltage"], axis=0)

# write to output file
print("Writing processed file to ", args.processedFile)
with open(args.processedFile, "wb") as outfile:
    df_data.to_csv(outfile, index=False)

# write average waveforms to file
print("Writing average waveforms to ", args.averageWaveformFile)
averageWaveformFile = ROOT.TFile(args.averageWaveformFile, "RECREATE")
for avg_wvf_gr in avg_waveform_graphs:
    avg_wvf_gr.Write()
averageWaveformFile.Close()

# write cells and voltages in the data to file
with open(args.phaseSpaceFile, "wb") as phaseSpaceFile:
    pickle.dump({
        "cells": np.array(df_data.Cell.unique()),
        "voltages": np.array(df_data.Voltage.unique())
    }, phaseSpaceFile)