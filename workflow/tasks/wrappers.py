import luigi, os, sys, shutil
from subprocess import Popen

from common.decorators import *
from common.tasks import MeasurementTask
from common.utility.fs import create_dir

from workflow.tasks.vis import *
from workflow.tasks.ana import *

from database.database import tct_db, dut_db


class AnalyseAllMeasurements(luigi.Task):
    task_namespace = 'TCT'

    def requires(self, delete_mode=False):
        tbr = []
        for MeasurementID in tct_db.get_allIDs():
            tbr.append(AnalyseMeasurement(MeasurementID=MeasurementID))
        return tbr

    def run(self):
        return True

    @DefineLocalTargets
    def output(self):   
        return "dummy.txt"


class AnalyseMeasurement(MeasurementTask):
    task_namespace = 'TCT'

    def requires(self, delete_mode=False):
        tbr = {
           "signal_vs_voltage": SignalVsVoltage(MeasurementID=self.MeasurementID), 
           "processed": AnalyseAverageWaveform(MeasurementID=self.MeasurementID),
           "cell_figures": MakeCellFiguresForAllCells(MeasurementID=self.MeasurementID)
        }
        sensor_name = tct_db.get_DUT(self.MeasurementID)
        if dut_db.get_geofile(sensor_name) != "-":
            tbr["ch_map"] = MakeHexPlotChannelMap(MeasurementID=self.MeasurementID)
            tbr["hexplots"] = MakeHexPlotsForAllBiasVoltages(MeasurementID=self.MeasurementID)
        return tbr 


    @DefineLocalTargets
    def output(self):
        out_directory = create_dir(os.path.join(FileDirectories["summaries"], tct_db.get_DUT(self.MeasurementID)+"_"+self.MeasurementID))
        return {
            "pdf": os.path.join(out_directory, "%s.pdf"%self.MeasurementID)
        }

    def run(self):
        sensor_name = tct_db.get_DUT(self.MeasurementID)
        measurement_temp = tct_db.get_temperature(self.MeasurementID)
        voltages = pickle.load(open(self.input()["processed"]["tested"].path, "r"))["voltages"]
        cells = pickle.load(open(self.input()["processed"]["tested"].path, "r"))["cells"]
        geofile = dut_db.get_geofile(sensor_name)


        #read and customise the main tex file
        main_tex_tex = open("%s/workflow/tex/main.tex"%os.environ["WORKFLOW_DIR"], "r").read()
        main_tex_tex = main_tex_tex.replace("<MEASUREMENTID>", self.MeasurementID.replace("_", "$\_$"))
        main_tex_tex = main_tex_tex.replace("<SENSORNAME>", sensor_name.replace("_", "$\_$"))
        main_tex_tex = main_tex_tex.replace("<DATE>", tct_db.get_date(self.MeasurementID))
        main_tex_tex = main_tex_tex.replace("<STATION>", tct_db.get_station(self.MeasurementID))
        main_tex_tex = main_tex_tex.replace("<TEMPERATURE>", str(tct_db.get_temperature(self.MeasurementID)))
        main_tex_tex = main_tex_tex.replace("<ATTENUATION>", str(tct_db.get_attenuation(self.MeasurementID)))
        main_tex_tex = main_tex_tex.replace("<OSCILLOSCOPESETTING>", str(tct_db.get_oscilloscopeSetting(self.MeasurementID)))
        main_tex_tex = main_tex_tex.replace("<DAQFILES>", str(tct_db.get_filedirectory(self.MeasurementID)).replace("_", "$\_$"))

        main_tex_tex = main_tex_tex.replace("<SENSORSIZE>", str(dut_db.get_size(sensor_name)))
        main_tex_tex = main_tex_tex.replace("<NCHANNELS>", str(dut_db.get_nchannels(sensor_name)))
        main_tex_tex = main_tex_tex.replace("<DOPING>", dut_db.get_doping(sensor_name))
        main_tex_tex = main_tex_tex.replace("<IRRADIATION>", str(dut_db.get_irradiation(sensor_name)))
        main_tex_tex = main_tex_tex.replace("<PSTOP>", dut_db.get_pstop(sensor_name))
        main_tex_tex = main_tex_tex.replace("<THICKNESS>", str(dut_db.get_thickness(sensor_name)))


        #read and customise the leakage current hexplots
        tct_hexplot_voltage_tex = ""
        if "hexplots" in self.input():
            for voltage in reversed(sorted(voltages, key=abs)):
                tct_hexplot_voltage_tex += open("%s/workflow/tex/hexplots.tex"%os.environ["WORKFLOW_DIR"], "r").read()
                tct_hexplot_voltage_tex = tct_hexplot_voltage_tex.replace("<BIASVOLTAGE>", str(voltage))        
                basepath = MakeHexPlotForBiasVoltage(MeasurementID=self.MeasurementID, BiasVoltage=voltage).output().path
                tct_hexplot_voltage_tex = tct_hexplot_voltage_tex.replace("<HEXPLOT_RAW_AMPLITUDE>", basepath.replace(".txt", "_Amplitude_raw.pdf"))
                tct_hexplot_voltage_tex = tct_hexplot_voltage_tex.replace("<HEXPLOT_RAW_INTEGRAL>", basepath.replace(".txt", "_Integral_raw.pdf"))
                tct_hexplot_voltage_tex = tct_hexplot_voltage_tex.replace("<HEXPLOT_RAW_RMS>", basepath.replace(".txt", "_Noise_raw.pdf"))
                tct_hexplot_voltage_tex = tct_hexplot_voltage_tex.replace("<HEXPLOT_AMPLIFIED_AMPLITUDE>", basepath.replace(".txt", "_Amplitude_amplified.pdf"))
                tct_hexplot_voltage_tex = tct_hexplot_voltage_tex.replace("<HEXPLOT_AMPLIFIED_INTEGRAL>", basepath.replace(".txt", "_Integral_amplified.pdf"))
                tct_hexplot_voltage_tex = tct_hexplot_voltage_tex.replace("<HEXPLOT_AMPLIFIED_RMS>", basepath.replace(".txt", "_Noise_amplified.pdf"))
                
                
        #read and customise channel pad numbering
        chmapping_tex = ""
        if "ch_map" in self.input():
            chmapping_tex = open("%s/workflow/tex/ch_mapping.tex"%os.environ["WORKFLOW_DIR"], "r").read()
            chmapping_tex = chmapping_tex.replace("<HEXPLOT_CH_MAP>", self.input()["ch_map"].path)

        #cell summary figures
        tct_cell_figure_tex = ""
        for cell in sorted(cells, key=abs):
            tct_cell_figure_tex += open("%s/workflow/tex/cell_figures.tex"%os.environ["WORKFLOW_DIR"], "r").read()
            tct_cell_figure_tex = tct_cell_figure_tex.replace("<CELL>", str(cell))        
            basepath = MakeCellFigures(MeasurementID=self.MeasurementID, CellNr=cell).output().path
            tct_cell_figure_tex = tct_cell_figure_tex.replace("<FIGURE>", basepath)

        #merge tex and write to temporary outputfile
        main_tex_tex = main_tex_tex.replace("<FRAMES_HEXPLOTVOLTAGE>", tct_hexplot_voltage_tex)
        main_tex_tex = main_tex_tex.replace("<FRAMES_CHMAP>", chmapping_tex)
        main_tex_tex = main_tex_tex.replace("<FRAMES_CELLFIGURES>", tct_cell_figure_tex)
    

        tmp_file_path = self.output()["pdf"].path.replace(".pdf", ".tex")
        with open(tmp_file_path, "w") as tmp_file:
            tmp_file.write(main_tex_tex)
        #make latex file
        cmd = "pdflatex -output-directory=%s %s" % (os.path.dirname(self.output()["pdf"].path), tmp_file_path)
        for it in range(2):     #need to compile twice to properly see the table of contents and page numbers
            p = Popen(cmd, stdout=sys.stdout, stderr=sys.stderr, shell=True)
            out, err = p.communicate()
            code = p.returncode
        #cleanup
        for file_extension in [".aux", ".log", ".nav", ".out", ".snm", ".toc"]:
            os.remove(tmp_file_path.replace(".tex", file_extension))

        
        #final step: copy everything to directory containing raw data
        rawFileDirectory = tct_db.get_filedirectory(self.MeasurementID)
        baseNamePDF = os.path.basename(self.output()["pdf"].path)
        if os.path.exists(rawFileDirectory):
            print "Copying pdf and elog file to",rawFileDirectory
            shutil.copyfile(self.output()["pdf"].path, os.path.join(rawFileDirectory, baseNamePDF))
            print "Copied",self.output()["pdf"].path, "to", os.path.join(rawFileDirectory, baseNamePDF)
        
        return True

    def command(self):
        pass