#To be adjusted!


#Thorben Quast, thorben.quast@cern.ch
#01 March 2021
#A script that removes all output files from a given task and all subsequent ones.
#the followings task outputs are never deleted
IGNOREFORDELETION = ["AnalyseAllMeasurements"]
#name of the top-most task
HEADTASKNAME = "AnalyseMeasurement"

import luigi, os, argparse, numpy
parser = argparse.ArgumentParser()
parser.add_argument("--MeasurementID", type=str, help="(parameter) ID of the measurement.", default="", required=True)
parser.add_argument("--DeleteAll", type=int, help="(parameter) Delete all files associated to the workflow? If==1, it has preference over the TaskName and Recursive options.", default=0, required=False)
parser.add_argument("--TaskName", type=str, help="(parameter) Indicate the task whose output and (if Recursive==1) the one from all subsequent tasks is to be deleted.", default="AnalyseMeasurement", required=True)
parser.add_argument("--Recursive", type=int, help="(parameter) Recursive deletion of files down to the indicated task name.", default=1, required=False)
parser.add_argument("--Force", type=bool, help="(parameter) Ommit user feedback before deletion", default=False, required=False)
args = parser.parse_args()

#get all implemented tasks
import workflow.tasks.ana
import workflow.tasks.vis
import workflow.tasks.wrappers
available_tasks = {}
for _module in [workflow.tasks.ana, workflow.tasks.vis, workflow.tasks.wrappers]:
	for _attr_name in dir(_module):
		if type(getattr(_module, _attr_name))==luigi.task_register.Register:
			if getattr(_module, _attr_name).task_namespace=="TCT":
				if _attr_name in IGNOREFORDELETION:
					continue
				available_tasks[_attr_name] = getattr(_module, _attr_name)


#top-most task
head_task = available_tasks[HEADTASKNAME](MeasurementID=args.MeasurementID)
all_tasks = []
def traverse_task(_t):
	all_tasks.append(_t)
	_req = _t.requires(delete_mode=True)
	if type(_req)==dict:
		for key in _req:
			_task = _req[key]
			traverse_task(_task)
	elif type(_req)==list:
		for _task in _req:
			traverse_task(_task)
	elif _req!=[]:
		if not _req in all_tasks:
			all_tasks.append(_req)
			traverse_task(_req)
traverse_task(head_task)
all_tasks = numpy.unique(all_tasks)

if not args.TaskName in available_tasks:
	import sys
	sys.exit("The task '"+args.TaskName+"'' is not implemented. Nothing to do. Quit!")

#find all tasks which depend on the indicated path
tasks_for_deletion = []
if args.DeleteAll:
	for _t in all_tasks:
		tasks_for_deletion.append(_t)
else:
	main_task_for_deletion = available_tasks[args.TaskName](MeasurementID=args.MeasurementID)
	tasks_for_deletion.append(main_task_for_deletion)
	del available_tasks[args.TaskName]
	if args.Recursive==1:
		def check_dependence_on_task(_candidate, _ref):
			_requirements = _candidate.requires(delete_mode=True)	
			if type(_requirements)==dict:
				for key in _requirements:
					_task = _requirements[key]
					if check_dependence_on_task(_task, _ref):
						return True
			elif type(_requirements)==list:
				if _requirements == []:
					return False
				else:
					for _task in _requirements:
						if check_dependence_on_task(_task, _ref):
							return True
			else:
				if _requirements==_ref:
					return True
				else:
					return check_dependence_on_task(_requirements, _ref)


		for delete_candidate_task in all_tasks:
			if check_dependence_on_task(delete_candidate_task, main_task_for_deletion):
				tasks_for_deletion.append(delete_candidate_task)



#prepare list of file paths to delete
def add_file_paths(_farray, _outputs):
	if type(_outputs)==dict:
		for key in _outputs:
			add_file_paths(_farray, _outputs[key])
	elif type(_outputs)==list:
		for _o in _outputs:
			add_file_paths(_farray, _o)
	else:
		try:
			_farray.append(_outputs.path)
		except:
			print("Error in appending output file paths for deletion.")
			print(_outputs)


_files_for_deletion = []
for task in tasks_for_deletion:
	_outputs = task.output()
	add_file_paths(_files_for_deletion, _outputs)
_existing_files_for_deletion = []


print("The following files are subject for deletion:")
for fpath in sorted(_files_for_deletion):
	if os.path.exists(fpath) and not fpath in _existing_files_for_deletion:
		_existing_files_for_deletion.append(fpath)
		print(fpath)
if not args.Force:
	conf_text = raw_input("Type 'yes' for deletion of these files: ")
else:
	conf_text="yes"
if conf_text=="yes":
	for fpath in sorted(_existing_files_for_deletion):
		print("Removing "+fpath)
		os.remove(fpath)
	print("Done.")
else:
	print("Cancelled.")
