import luigi
import os
import pickle

from common.decorators import *
from common.tasks import MeasurementTask

from database.database import tct_db, dut_db
from workflow.conf.output import FileDirectories


class MakeCellFiguresForAllCells(MeasurementTask):
    task_namespace = 'TCT'

    def requires(self, delete_mode=False):
        from workflow.tasks.ana import AnalyseAverageWaveform
        inputFileCandidate = AnalyseAverageWaveform(
            MeasurementID=self.MeasurementID).output()["tested"].path
        if os.path.exists(inputFileCandidate):
            cells = pickle.load(open(inputFileCandidate, "r"))["cells"]
            return [MakeCellFigures(MeasurementID=self.MeasurementID, CellNr=int(_cell)) for _cell in cells]
        else:
            if delete_mode:
                return []
            else:
                AnalyseAverageWaveform(
                    MeasurementID=self.MeasurementID).manual_yield()
                cells = pickle.load(open(inputFileCandidate, "r"))["cells"]
                return [MakeCellFigures(MeasurementID=self.MeasurementID, CellNr=int(_cell)) for _cell in cells]

    @ AddOutputDirectory(FileDirectories["cell_figures"])
    @ DefineLocalTargets
    def output(self):
        return "%s_cell_figure_paths.txt" % self.MeasurementID

    def run(self):
        outfile_content = ""
        for inp in self.input():
            outfile_content += inp.path+"\n"

        with open(self.output().path, "w") as outfile:
            outfile.write(outfile_content)

    def command(self):
        pass


class MakeCellFigures(MeasurementTask):
    task_namespace = 'TCT'
    CellNr = luigi.IntParameter(
        default=99, description="Cell number to visualise", significant=True)

    def requires(self, delete_mode=False):
        from workflow.tasks.ana import AnalyseAverageWaveform
        return {
            "processed": AnalyseAverageWaveform(MeasurementID=self.MeasurementID),
            "curves": SignalVsVoltage(MeasurementID=self.MeasurementID)
        }

    @AddOutputDirectory(FileDirectories["cell_figures"], ID=MeasurementTask.get_MeasurementID)
    @DefineLocalTargets
    def output(self):
        return "cell%i.pdf" % self.CellNr

    @PythonCommand(os.path.join(os.environ["WORKFLOW_DIR"], "workflow/visualisation/channel_figures.py"))
    @UnpackPythonOptions
    def command(self):
        voltages = sorted(pickle.load(
            open(self.input()["processed"]["tested"].path, "r"))["voltages"])
        return {
            "averageWaveforms": self.input()["processed"]["waveforms"].path,
            "signalVsVoltage": self.input()["curves"].path,
            "outputFile": self.output().path,
            "measurementID": self.MeasurementID,
            "Cell": self.CellNr,
            "Voltage1": 25,
            "Voltage2": 250,
            "Voltage3": 500
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        return True


class SignalVsVoltage(MeasurementTask):
    task_namespace = 'TCT'

    def requires(self, delete_mode=False):
        from workflow.tasks.ana import AnalyseAverageWaveform
        return AnalyseAverageWaveform(MeasurementID=self.MeasurementID)

    @AddOutputDirectory(FileDirectories["signal_vs_voltage"], ID=MeasurementTask.get_MeasurementID)
    @DefineLocalTargets
    def output(self):
        return "signal_vs_voltage.root"

    @PythonCommand(os.path.join(os.environ["WORKFLOW_DIR"], "workflow/visualisation/signal_vs_voltage.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "inputFile": self.input()["quantities"].path,
            "measurementID": self.MeasurementID,
            "outfileROOTPath": self.output().path
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        return True


class MakeHexPlotChannelMap(MeasurementTask):
    task_namespace = 'TCT'

    @AddOutputDirectory(FileDirectories["hexplots"], ID=MeasurementTask.get_MeasurementID)
    @DefineLocalTargets
    def output(self):
        return "channel_map.pdf"

    @PythonCommand(os.path.join(os.environ["WORKFLOW_DIR"], "workflow/visualisation/channel_mapping.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "mappingFile": self.output().path,
            "measurementID": self.MeasurementID
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        return True


class MakeHexPlotsForAllBiasVoltages(MeasurementTask):
    task_namespace = 'TCT'

    def requires(self, delete_mode=False):
        from workflow.tasks.ana import AnalyseAverageWaveform
        inputFileCandidate = AnalyseAverageWaveform(
            MeasurementID=self.MeasurementID).output()["tested"].path
        if os.path.exists(inputFileCandidate):
            voltages = pickle.load(open(inputFileCandidate, "r"))["voltages"]
            return [MakeHexPlotForBiasVoltage(MeasurementID=self.MeasurementID, BiasVoltage=int(v)) for v in voltages]
        else:
            if delete_mode:
                return []
            else:
                AnalyseAverageWaveform(
                    MeasurementID=self.MeasurementID).manual_yield()
                voltages = pickle.load(open(inputFileCandidate, "r"))[
                    "voltages"]
                return [MakeHexPlotForBiasVoltage(MeasurementID=self.MeasurementID, BiasVoltage=int(v)) for v in voltages]

    @ AddOutputDirectory(FileDirectories["hexplots"])
    @ DefineLocalTargets
    def output(self):
        return "%s_hexplot_paths.txt" % self.MeasurementID

    def run(self):
        outfile_content = ""
        for inp in self.input():
            outfile_content += inp.path+"\n"

        with open(self.output().path, "w") as outfile:
            outfile.write(outfile_content)

    def command(self):
        pass


class MakeHexPlotForBiasVoltage(MeasurementTask):
    task_namespace = 'TCT'
    BiasVoltage = luigi.IntParameter(
        default=200, description="Applied bias voltage during the measurement", significant=True)

    def requires(self, delete_mode=False):
        from workflow.tasks.ana import AnalyseAverageWaveform
        return AnalyseAverageWaveform(MeasurementID=self.MeasurementID)

    @ AddOutputDirectory(FileDirectories["hexplots"], ID=MeasurementTask.get_MeasurementID)
    @ DefineLocalTargets
    def output(self):
        return "%iV.txt" % self.BiasVoltage

    @ PythonCommand(os.path.join(os.environ["WORKFLOW_DIR"], "workflow/visualisation/hexplots.py"))
    @ UnpackPythonOptions
    def command(self):
        return {
            "inputFile": self.input()["quantities"].path,
            "outputFile": self.output().path,
            "measurementID": self.MeasurementID,
            "biasVoltage": self.BiasVoltage
        }

    @ SourceROOT
    @ Subprocess(command)
    def run(self):
        return True
