import luigi, os

from common.decorators import *
from common.tasks import MeasurementTask

from database.database import tct_db, dut_db
from workflow.conf.output import FileDirectories


class AnalyseAverageWaveform(MeasurementTask):
    task_namespace = 'TCT'

    @AddOutputDirectory(FileDirectories["ana_wvf"], ID=MeasurementTask.get_MeasurementID)
    @DefineLocalTargets
    def output(self):   
        return {
            "quantities": "derived_quantities.csv",
            "waveforms": "waveform_graphs.root",
            "tested": "tested.pkl",
        }

    @PythonCommand(os.path.join(os.environ["WORKFLOW_DIR"], "workflow/analysis/analyse_processed_data.py"))
    @UnpackPythonOptions
    def command(self):
        return {
            "infilePath": tct_db.get_filedirectory(self.MeasurementID),
            "fileNamePattern": tct_db.get_rawfilePattern(self.MeasurementID),
            "measurementID": self.MeasurementID,
            "averageWaveformFile": self.output()["waveforms"].path,
            "processedFile": self.output()["quantities"].path,
            "phaseSpaceFile": self.output()["tested"].path,
        }

    @SourceROOT
    @Subprocess(command)
    def run(self):
        if os.path.exists(self.output()["quantities"].path):
            return False
        else:
            return True     