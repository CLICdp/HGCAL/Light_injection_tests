# Author: Thorben Quast, thorben.quast@cern.ch
# Date: 01 March 2021
# Description:
# 

import argparse
from copy import deepcopy

from common.utility.tgraph_helpers import scale_graph
import ROOT
ROOT.gROOT.SetBatch(True)


parser = argparse.ArgumentParser()
parser.add_argument("--averageWaveforms", type=str, help="(input)",
                    default="/home/tquast/tct_Spring2021/ana_wvf/8inch_LD_1104_neg20deg_1p8Vatt_0neq/waveform_graphs.root", required=True)
parser.add_argument("--signalVsVoltage", type=str, help="(input)",
                    default="/home/tquast/tct_Spring2021/signal_vs_voltage/8inch_LD_1104_neg20deg_1p8Vatt_0neq/signal_vs_voltage.root", required=True)
parser.add_argument("--outputFile", type=str, help="(Output) Path to the output png",
                    default="/afs/cern.ch/user/t/tquast/Desktop/test_cell99.png", required=True)
parser.add_argument("--measurementID", type=str,
                    help="Indicate measurement type for labels", default="", required=True)
parser.add_argument("--Cell", type=int,
                    help="Which cell to visualise", default=99, required=True)
parser.add_argument("--Voltage1", type=int,
                    help="Indicate first bias voltage to visualise", default=0, required=True)
parser.add_argument("--Voltage2", type=int,
                    help="Indicate first bias voltage to visualise", default=150, required=True)
parser.add_argument("--Voltage3", type=int,
                    help="Indicate second bias voltage to visualise", default=500, required=True)
args = parser.parse_args()


# read the average waveforms
avg_waveform_file = ROOT.TFile(args.averageWaveforms, "READ")
average_waveform_raw_U1 = deepcopy(avg_waveform_file.Get(
    "average_waveform_cell%i_%iV_raw" % (args.Cell, args.Voltage1)))
average_waveform_raw_U2 = deepcopy(avg_waveform_file.Get(
    "average_waveform_cell%i_%iV_raw" % (args.Cell, args.Voltage2)))
average_waveform_raw_U3 = deepcopy(avg_waveform_file.Get(
    "average_waveform_cell%i_%iV_raw" % (args.Cell, args.Voltage3)))
average_waveform_amplified_U1 = deepcopy(avg_waveform_file.Get(
    "average_waveform_cell%i_%iV_amplified" % (args.Cell, args.Voltage1)))
average_waveform_amplified_U2 = deepcopy(avg_waveform_file.Get(
    "average_waveform_cell%i_%iV_amplified" % (args.Cell, args.Voltage2)))
average_waveform_amplified_U3 = deepcopy(avg_waveform_file.Get(
    "average_waveform_cell%i_%iV_amplified" % (args.Cell, args.Voltage3)))
avg_waveform_file.Close()

# read the signal vs voltage curves
signal_vs_voltage_file = ROOT.TFile(args.signalVsVoltage, "READ")
amplitude_raw = deepcopy(signal_vs_voltage_file.Get("signal_amplitude_cell%i_raw" % args.Cell))
integral_raw = deepcopy(signal_vs_voltage_file.Get("signal_integral_cell%i_raw" % args.Cell))
noise_raw = deepcopy(signal_vs_voltage_file.Get("noise_cell%i_raw" % args.Cell))
amplitude_amplified = deepcopy(signal_vs_voltage_file.Get("signal_amplitude_cell%i_amplified" % args.Cell))
integral_amplified = deepcopy(signal_vs_voltage_file.Get("signal_integral_cell%i_amplified" % args.Cell))
noise_amplified = deepcopy(signal_vs_voltage_file.Get("noise_cell%i_amplified" % args.Cell))
signal_vs_voltage_file.Close()


#
def plot_waveforms(w1, w2, w3, label):
    legend = ROOT.TLegend(0.7, 0.6, 0.9, 0.9)
    legend.SetHeader("U_{bias} [V]")
    try:
        yminimum = min([ROOT.TMath.MinElement(gr.GetN(), gr.GetY()) for gr in [w1, w2, w3]])
        ymaximum = max([ROOT.TMath.MaxElement(gr.GetN(), gr.GetY()) for gr in [w1, w2, w3]])
        w1.SetLineColor(ROOT.kGreen+1)
        w1.SetLineWidth(2)
        w1.SetMarkerColor(ROOT.kGreen+1)
        w1.SetMarkerSize(0)
        w2.SetLineColor(ROOT.kBlue+1)
        w2.SetLineWidth(2)
        w2.SetMarkerColor(ROOT.kBlue+1)
        w2.SetMarkerSize(0)
        w3.SetLineColor(ROOT.kBlack)
        w3.SetLineWidth(2)
        w3.SetMarkerColor(ROOT.kBlack)
        w3.SetMarkerSize(0)
        w1.GetYaxis().SetRangeUser(yminimum, ymaximum)
        w1.GetXaxis().SetTitle("Sample index")
        w1.GetYaxis().SetTitle("Average signal [mV]")
        w1.SetTitle("Average waveform: Cell %i (%s)" % (args.Cell, label))
        w1.GetXaxis().SetTitleSize(1.2*w1.GetXaxis().GetTitleSize())
        w1.GetYaxis().SetTitleSize(1.2*w1.GetYaxis().GetTitleSize())
        w1.Draw("APL")
        w2.Draw("PLSAME")
        w3.Draw("PLSAME")  
        legend.AddEntry(w1, "%i" % args.Voltage1, "l")
        legend.AddEntry(w2, "%i" % args.Voltage2, "l")
        legend.AddEntry(w3, "%i" % args.Voltage3, "l")
        legend.Draw()
    except:
        pass
    return legend  

# prepare the output canvas
canvas = ROOT.TCanvas("canvas", "canvas", 2400, 1200)
legends = []
canvas.Divide(3, 2)
canvas.SetGrid(True)

canvas.cd(1)
legends.append(plot_waveforms(average_waveform_raw_U1, average_waveform_raw_U2, average_waveform_raw_U3, "raw"))

canvas.cd(4)
legends.append(plot_waveforms(average_waveform_amplified_U1, average_waveform_amplified_U2, average_waveform_amplified_U3, "amplified"))



def draw_curves(gr1, gr2, label):
    if label=="Noise":
        legend = ROOT.TLegend(0.55, 0.65, 0.9, 0.9)
    else:
        legend = ROOT.TLegend(0.55, 0.15, 0.9, 0.4)
    legend.SetHeader(label+" vs. U_{bias}")
    try:
        scale_gr2 = ROOT.TMath.MaxElement(gr1.GetN(), gr1.GetY()) / ROOT.TMath.MaxElement(gr2.GetN(), gr2.GetY())
        scale_graph(gr2, scale_gr2)
        gr1.SetLineColor(ROOT.kBlack)
        gr1.SetLineWidth(3)
        gr1.SetMarkerColor(ROOT.kBlack)
        gr2.SetLineColor(ROOT.kBlue+1)
        gr2.SetLineWidth(3)
        gr2.SetMarkerColor(ROOT.kBlue+1)
        gr1.SetTitle("Cell %i (%s)" % (args.Cell, label))
        gr1.Draw("APL")
        gr2.Draw("PLSAME")
        legend.AddEntry(gr1, "raw [x1]", "l")
        legend.AddEntry(gr2, "amplified [x1/%.1f]" % (1/scale_gr2), "l")
        legend.Draw()
    except:
        pass
    return legend

canvas.cd(2)
legends.append(draw_curves(amplitude_raw, amplitude_amplified, "Amplitude"))

canvas.cd(3)
legends.append(draw_curves(integral_raw, integral_amplified, "Integral"))

canvas.cd(5)
legends.append(draw_curves(noise_raw, noise_amplified, "Noise"))

canvas.Print(args.outputFile)