# Author: Thorben Quast, thorben.quast@cern.ch
# Date: 01 March 2021
# Description:
# produces the hexplot channel numbering visualisation

from database.database import tct_db, dut_db

import os
import sys
from subprocess import Popen

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--mappingFile", type=str,
                    help="(output) Output file showing the hexplot numbering scheme.", default="", required=True)
parser.add_argument("--measurementID", type=str,
                    help="Indicate measurement type for labels", default="", required=True)
args = parser.parse_args()

# read DB information
dut_name = tct_db.get_DUT(args.measurementID)
hexplot_geo_file = dut_db.get_geofile(dut_name)

# hexplot plotting
HEXPLOTCOMMAND = "<HEXPLOTDIR>/HexPlot \
	    -g <HEXPLOTDIR>/geo/<GEOFILE> \
	    -o <OUTPUTFILE> \
	    -p GEO --pn 0".replace("<HEXPLOTDIR>", os.environ["HEXPLOT_DIR"]).replace("<GEOFILE>", hexplot_geo_file).replace("<OUTPUTFILE>", args.mappingFile)

env = {}
env.update(os.environ)
p = Popen(HEXPLOTCOMMAND, stdout=sys.stdout,
          stderr=sys.stderr, shell=True, env=env)
out, err = p.communicate()
code = p.returncode

if code != 0:
    raise Exception("Hexplot plotting failed")
