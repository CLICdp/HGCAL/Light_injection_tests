# Author: Thorben Quast, thorben.quast@cern.ch
# Created: 25 Feb 2021
# Last modified: 25 Feb 2021
# Description:
#

import argparse
import pandas as pd
import numpy as np
import os
from tqdm import tqdm
from common.utility import fs

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")  # disable info messages
ROOT.gStyle.SetOptFit()
ROOT.gStyle.SetOptStat()

parser = argparse.ArgumentParser()
parser.add_argument('--inputFile', type=str, default="/home/tquast/TCT_2021/8inch_LD_1104_neg20deg_1p8Vatt_0neq/processed.csv",
                    help='Input file containing the processed waveform quantities', required=True)
parser.add_argument("--measurementID", type=str,
                    help="Indicate measurement type for labels", default="", required=True)
parser.add_argument("--outfileROOTPath", type=str, default="/home/tquast/TCT_2021/8inch_LD_1104_neg20deg_1p8Vatt_0neq/processed.root",
                    help="(output) Path to the root file containing the waveform graphs for later visualisation.", required=True)
args = parser.parse_args()


def fill_graph(gr, df_x, df_y, df_yerr):
    _x = np.array(df_x)
    _y = np.array(df_y)
    _yerr = np.array(df_yerr)
    for npoint in range(len(_x)):
        gr.SetPoint(npoint, _x[npoint], _y[npoint])
        gr.SetPointError(npoint, 0., _yerr[npoint])


def graph_labels(gr, quantity, _type):
    gr.SetTitle(gr.GetName())

    gr.GetXaxis().SetTitle("|U_{bias}| (no correction applied) [V]")

    y_axis_title = "(%s)" % _type
    if quantity == "noise":
        y_axis_title = "Noise from baseline fluctuation " + \
            y_axis_title + " [mV]"
    elif quantity == "amplitude":
        y_axis_title = "Signal amplitude from parabola fit " + \
            y_axis_title + " [mV]"
    elif quantity == "integral":
        y_axis_title = "Signal integral from sample sum " + \
            y_axis_title + " [mV ns]"
    gr.GetYaxis().SetTitle(y_axis_title)

    gr.GetXaxis().SetTitleSize(1.2*gr.GetXaxis().GetTitleSize())
    gr.GetYaxis().SetTitleSize(1.2*gr.GetYaxis().GetTitleSize())

    gr.GetXaxis().SetLabelSize(1.2*gr.GetXaxis().GetLabelSize())
    gr.GetYaxis().SetLabelSize(1.2*gr.GetYaxis().GetLabelSize())

    gr.SetLineWidth(2)
    gr.SetMarkerSize(2)


# create output directory
outfileROOTPath = args.outfileROOTPath
outfileROOTDirectory = fs.create_dir(os.path.dirname(args.outfileROOTPath))

# prepare output ROOT file
outfile = ROOT.TFile(outfileROOTPath, "RECREATE")
outfile.Close()

# read the processed data
inputData = pd.read_csv(args.inputFile)

# prepare dictionary that temporarily stores the TGraphErrors
_tgraphs = {}

# loop over all cells in the dataframe
for _cell_data in tqdm(inputData.groupby(inputData.Cell, axis=0), unit="cells"):
    _cell = _cell_data[0]
    cell_data = _cell_data[1]
    _tgraphs[_cell] = {}
    for _type in ["raw", "amplified"]:
        _tgraphs[_cell][_type] = {}

        cell_data_type = cell_data[cell_data.Type == _type]
        cell_data_type = cell_data_type.sort_values(["Voltage"], axis=0)

        gr_signal_amplitude = ROOT.TGraphErrors()
        gr_signal_amplitude.SetName(
            "signal_amplitude_cell%i_%s" % (_cell, _type))
        gr_signal_integral = ROOT.TGraphErrors()
        gr_signal_integral.SetName(
            "signal_integral_cell%i_%s" % (_cell, _type))
        gr_noise = ROOT.TGraphErrors()
        gr_noise.SetName("noise_cell%i_%s" % (_cell, _type))

        fill_graph(gr_signal_amplitude, cell_data_type.Voltage,
                   cell_data_type.Amplitude, cell_data_type.Amplitude_err)
        fill_graph(gr_signal_integral, cell_data_type.Voltage,
                   cell_data_type.Integral, cell_data_type.Integral_err)
        fill_graph(gr_noise, cell_data_type.Voltage,
                   cell_data_type.Noise, cell_data_type.Noise_err)

        _tgraphs[_cell][_type]["amplitude"] = gr_signal_amplitude
        _tgraphs[_cell][_type]["integral"] = gr_signal_integral
        _tgraphs[_cell][_type]["noise"] = gr_noise


# write all graphs to ROOT file
print("Writing graphs to", outfileROOTPath)
outfile = ROOT.TFile(outfileROOTPath, "UPDATE")
for _cell in _tgraphs:
    for _type in _tgraphs[_cell]:
        for _quantity in _tgraphs[_cell][_type]:
            graph_labels(_tgraphs[_cell][_type][_quantity], _quantity, _type)
            _tgraphs[_cell][_type][_quantity].Write()
outfile.Close()


# overlay all graphs and write to one canvas
canvas = ROOT.TCanvas("canvas", "canvas", 3200, 2700)
canvas.Divide(2, 3)

for _cell_index, _cell in enumerate(_tgraphs):
    for _type_index, _type in enumerate(_tgraphs[_cell]):
        for _quantity_index, _quantity in enumerate(_tgraphs[_cell][_type]):
            canvas.cd(1+2*_quantity_index+_type_index)
            _tgraphs[_cell][_type][_quantity].SetLineColor(_cell_index % 30)
            _tgraphs[_cell][_type][_quantity].SetMarkerColor(_cell_index % 30)
            draw_option = "PLSAME"
            if _cell_index == 0:
                _tgraphs[_cell][_type][_quantity].SetTitle(
                    "%s (%s)" % (_quantity, _type))
                draw_option = "APL"
            _tgraphs[_cell][_type][_quantity].Draw(draw_option)
outfilePDFPath = outfileROOTPath.replace(".root", ".pdf")
print("Printing graphs to", outfilePDFPath)
canvas.Print(outfilePDFPath)
