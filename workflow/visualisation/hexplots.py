# Author: Thorben Quast, thorben.quast@cern.ch
# Date: 01 March 2021
# Description:
#

# standard imports
import numpy as np
import pandas as pd

import os
import sys
from scipy.stats import iqr
from subprocess import Popen

from database.database import tct_db, dut_db

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--inputFile", type=str, help="(input) Path to the processed csv file.",
                    default="/home/tquast/TCT_2021/8inch_LD_1104_neg20deg_1p8Vatt_0neq/processed.csv", required=True)
parser.add_argument("--outputFile", type=str, help="(Output) Path to the output pdf",
                    default="/home/tquast/TCT_2021/8inch_LD_1104_neg20deg_1p8Vatt_0neq/processed_500V.txt", required=True)
parser.add_argument("--measurementID", type=str,
                    help="Indicate measurement type for labels", default="", required=True)
parser.add_argument("--biasVoltage", type=int,
                    help="Indicate bias voltage type for labels", default=500, required=True)
args = parser.parse_args()

sensor_name = tct_db.get_DUT(args.measurementID)
hexplot_geo_file = dut_db.get_geofile(sensor_name)
N_channels = dut_db.get_nchannels(sensor_name)

# read data
dtypes = [("Cell", "i4"), ("Voltage", "i8"), ("Type", "S30")]
dtypes += [("Amplitude", "f8"), ("Amplitude_err", "f8")]
dtypes += [("Integral", "f8"), ("Integral_err", "f8")]
dtypes += [("Noise", "f8"), ("Noise_err", "f8")]
measurement_data = np.genfromtxt(
    args.inputFile, skip_header=1, delimiter=",", dtype=dtypes)
measurement_data = pd.DataFrame(measurement_data)
measurement_data = measurement_data[measurement_data.Cell <= N_channels]


# plotting with Hexplot
HEXPLOTCOMMAND = "<HEXPLOTDIR>/HexPlot \
	    -g <HEXPLOTDIR>/geo/<GEOFILE> \
	    -i <INPUTFILE> \
	    -o <OUTPUTFILE> \
	    --if <FORMAT> \
	    --CV -p GEO  --colorpalette <COLORPALETTE>\
	    --vn 'Signal:<DEF>:<UNIT>' --select <VOLTAGE>\
	    -z <ZMIN>:<ZMAX>".replace("<HEXPLOTDIR>", os.environ["HEXPLOT_DIR"]).replace("<GEOFILE>", hexplot_geo_file)

iterations = [("raw", "Amplitude"), ("raw", "Integral"), ("raw", "Noise")]
iterations += [("amplified", "Amplitude"),
               ("amplified", "Integral"), ("amplified", "Noise")]

for _type, _quantity in iterations:
    data_skimmed = measurement_data[measurement_data.Type == _type]
    data_skimmed = data_skimmed[measurement_data.Voltage == args.biasVoltage]
    data_skimmed = data_skimmed[["Cell", "Voltage", _quantity]]
    # write out to temporary files
    tmp_file_path = args.outputFile
    if os.path.exists(tmp_file_path):
        os.remove(tmp_file_path)
    with open(tmp_file_path, "w") as tmp_file:
        tmp_file.write(data_skimmed.to_string())

    pdfPath = args.outputFile.replace(
        ".txt", "_%s_%s.pdf" % (_quantity, _type))

    this_cmd = HEXPLOTCOMMAND
    this_cmd = this_cmd.replace("<INPUTFILE>", tmp_file_path)
    this_cmd = this_cmd.replace("<OUTPUTFILE>", pdfPath)
    this_cmd = this_cmd.replace("<VOLTAGE>", str(args.biasVoltage))
    this_cmd = this_cmd.replace("<COLORPALETTE>", "57")
    this_cmd = this_cmd.replace("<UNIT>", "a.u.")
    this_cmd = this_cmd.replace("<FORMAT>", "nul:PADNUM:SELECTOR:VAL")
    this_cmd = this_cmd.replace("<DEF>", "%s (%s)" % (_quantity, _type))

    median = np.median(data_skimmed[_quantity])
    IQR = iqr(np.array(data_skimmed[_quantity]), rng=(50, 84))
    # set the maximum scale: median +/-2.0 x rhs IQR68
    this_cmd = this_cmd.replace("<ZMIN>", str(abs(median)-2.*IQR))
    this_cmd = this_cmd.replace("<ZMAX>", str(abs(median)+2.*IQR))

    env = {}
    env.update(os.environ)
    p = Popen(this_cmd, stdout=sys.stdout,
              stderr=sys.stderr, shell=True, env=env)
    out, err = p.communicate()
    code = p.returncode

    if code != 0:
        raise Exception("Hexplot plotting failed")
