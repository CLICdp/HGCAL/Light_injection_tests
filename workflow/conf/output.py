import os

# path for the reconstructed/analysed files
BASEPATH = os.environ["TMPFILES_DIR_TCT"]
FileDirectories = {
	"ana_wvf": os.path.join(BASEPATH, "ana_wvf"),
	"signal_vs_voltage": os.path.join(BASEPATH, "signal_vs_voltage"), 
	"hexplots": os.path.join(BASEPATH, "hexplots"), 
	"cell_figures": os.path.join(BASEPATH, "cell_figures"), 
	"summaries": "/afs/cern.ch/user/t/tquast/Desktop/TCT_Spring2021"
}
