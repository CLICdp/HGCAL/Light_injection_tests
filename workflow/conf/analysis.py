OSCILLOSCOPESETTINGS = {}

OSCILLOSCOPESETTINGS["Default"] = {
    "NSSCALE": 80,                #ps
    "SIGNALRANGE_MIN": 200,       #samples
    "SIGNALRANGE_MAX": 650,       #samples
    "NOISERANGE_MIN": 0,          #samples
    "NOISERANGE_MAX": 150        #samples
}
