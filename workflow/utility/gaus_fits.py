import ROOT

def repeatedGausFit(histogram, gaus, rangeInSigmaLeft=1.0, rangeInSigmaRight=2.5, maxRange=-1):
	gaus.SetRange(histogram.GetXaxis().GetXmin(), histogram.GetXaxis().GetXmax())

	if rangeInSigmaRight==None:
		rangeInSigmaRight = rangeInSigmaLeft

	if histogram.GetEntries()==0:
		return

	ymaximum = histogram.GetMaximum()
	gaus.SetParameter(1, histogram.GetMean())
	gaus.SetParameter(2, histogram.GetStdDev())
	gaus.SetLineColor(histogram.GetLineColor())
	RMS = histogram.GetStdDev()
	gaus.SetParLimits(1, histogram.GetMean()-rangeInSigmaLeft*RMS, histogram.GetMean()+rangeInSigmaRight*RMS)
	if maxRange>RMS:
		gaus.SetParLimits(1, histogram.GetMean()-maxRange, histogram.GetMean()+maxRange)

	histogram.Fit(gaus, "QRN")
	for f_i in range(9):
		mu_tmp = gaus.GetParameter(1)
		sigma_tmp = gaus.GetParameter(2)
		gaus.SetRange(mu_tmp-rangeInSigmaLeft*sigma_tmp, mu_tmp+rangeInSigmaRight*sigma_tmp)
		if sigma_tmp>maxRange:
			gaus.SetRange(mu_tmp-maxRange, mu_tmp+maxRange)
		gaus.SetParameter(1, mu_tmp)
		gaus.SetParameter(2, sigma_tmp)
		histogram.Fit(gaus, "QRN")
	histogram.Fit(gaus, "QR")