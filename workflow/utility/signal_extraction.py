import ROOT
import numpy as np
from math import sqrt

# amplitude estimation
def compute_amplitude(_y, _y_err, gr, NSSCALE=80.):
    for n in range(len(_y)):
        gr.SetPoint(n, n*1., _y[n])
        gr.SetPointError(n, 0., _y_err[n])

    max_x_idx = np.argmax(_y)
    max_fit_pol2 = ROOT.TF1(
        "pol2_amp_fit", "pol2", max_x_idx-7., max_x_idx+11.)
    for par_index, par_name in enumerate(["c", "b", "a"]):
        max_fit_pol2.SetParName(par_index, par_name)
    fit_res = gr.Fit(max_fit_pol2, "RQS")
    cov_matrix = fit_res.GetCovarianceMatrix()
    a = max_fit_pol2.GetParameter(2)
    b = max_fit_pol2.GetParameter(1)
    c = max_fit_pol2.GetParameter(0)
    a_err2 = cov_matrix[2][2]
    b_err2 = cov_matrix[1][1]
    c_err2 = cov_matrix[0][0]
    ab_err = cov_matrix[1][2]
    ac_err = cov_matrix[0][2]
    bc_err = cov_matrix[0][1]

    amplitude = -b**2/a/4. + c
    ddc = 1
    ddb = -b/2./a
    dda = -b**2/4./a**2

    amplitude_error2 = (ddc**2)*c_err2 + (ddb**2)*b_err2 + (dda**2)*a_err2

    # include correlations
    amplitude_error2 += abs(dda*ddc)*ac_err
    amplitude_error2 += abs(ddb*ddc)*bc_err
    amplitude_error2 += abs(dda*ddb)*ab_err

    # TODO: Proper error propagation
    amplitude_error = 0  # sqrt(amplitude_error2)

    return amplitude, amplitude_error


# integral definition
def compute_integral(_y, _y_err, range_min=-1, range_max=-1, NSSCALE=80.):  # NSSCALE in ps
    _NSSCALE = NSSCALE/1000.  # conversion to ns
    __y = _y
    __y_err = _y_err
    if range_min != -1:
        __y = __y[range_min:]
        __y_err = __y_err[range_min:]
    if range_max != -1:
        __y = __y[:range_max]
        __y_err = __y_err[:range_max]

    return np.sum(__y)*_NSSCALE, np.sqrt(np.sum(__y_err))*_NSSCALE
