import luigi
from abc import abstractmethod



class MeasurementTask(luigi.Task):
    MeasurementID = luigi.Parameter(default="", description="ID of the measurement", significant=True)

    def get_MeasurementID(self):
        return self.MeasurementID

    @abstractmethod
    def output(self):
        pass

    @abstractmethod
    def run(self):
        pass        

    @abstractmethod
    def command(self):
        pass    

    def requires(self, delete_mode=False):
        return []

    #run a task manually, e.g. when yielded in a requirement function
    def manual_yield(self):
        _requirements = self.requires()
        if type(_requirements)==dict:
            for _req in _requirements:
                if not _req.complete():
                    _req.manual_yield()
                    _req.run()  
        elif type(_requirements)==list:
            for _req in _requirements:
                if not requirements[_req].complete():
                    requirements[_req].manual_yield()
                    requirements[_req].run()
        else:
            _req = _requirements
            if not _req.complete():
                _req.manual_yield()
                _req.run()            
        if not self.complete():
            self.run()