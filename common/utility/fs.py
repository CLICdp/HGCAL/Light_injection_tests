import os
def create_dir(_dir):
	if not os.path.exists(_dir):
		mother_dir = "/".join(_dir.split("/")[0:-1])
		if not os.path.exists(mother_dir):
			create_dir(mother_dir)
		os.mkdir(_dir)
	return _dir	