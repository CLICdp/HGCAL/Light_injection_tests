import ROOT
def scale_graph(gr, scale=1.):
    for i in range(gr.GetN()):
        gr.GetY()[i] *= scale
        gr.SetPointError(i, gr.GetErrorX(i), gr.GetErrorY(i) * scale)