# WORK IN PROGRESS !!!
# TCT Analysis Workflows for the HGCAL Silicon Sensor Characterisations at CERN 
## README to be written

* **Start of project:** 13 Nov 2020
* **Most recent update:** 13 Nov 2020
* **Created by:** Thorben Quast (thorben.quast@cern.ch)
* **Currently maintained by:** Thorben Quast (thorben.quast@cern.ch)

## Scope



### Supported Measurements


### Usage

The following outlines the ideal sequence of actions of how the framework is interfaced to the measurement efforts.



## Framework Structure



### Target-Based Analysis Workflow

Luigi is a data pipelining tool that helps in creating analysis workflows with intermediate files and in resolving dependencies between analysis steps in a natural way.
A nice and more detailed explanation including an illustrative example is given in this [blog post](https://intoli.com/blog/luigi-jupyter-notebooks/).
Motivated by [this application](https://www.researchgate.net/publication/317356570_Design_and_Execution_of_make-like_distributed_Analyses_based_on_Spotify's_Pipelining_Package_Luigi), luigi has also been used for the steering of the HGCAL beam test data reconstruction, see [Chapter 7.1 of T.Q.'s doctoral thesis draft](http://tquast.web.cern.ch/tquast/thesis_draft.pdf).

The main idea is to implement the analysis in atomistic and generic steps. The steps themselves may be written in any programming language. 
Every step is then wrapped into a luigi task which can be initiated via the command line.

### Directory Structure



## Installation



**Unix-like* operating systems** are assumed. Dependencies to external softwares are stated below. 




### Software Dependencies

This workflow makes use of the following software which need to be installed beforehand. The numbers indicated the versions with which the framework is being tested with.

**_Operating system_**

* Unix-like, no Windows
* Framework developed and tested on **CentOS7**

**_Python packages_**

* __**Python 2.7**__! 
* **luigi** ([readthedocs](https://luigi.readthedocs.io/en/stable/)) for workflow management.
* **ROOT6.06.06** ([website](https://root.cern.ch)) = default HEP data analysis and visualisation framework.
* **numpy v1.16** ([website](https://numpy.org)) = widely-used package for scientific computing with Python.
* **root_numpy** ([website](http://scikit-hep.org/root_numpy/)) for interfacing ROOT-TTrees to numpy arrays.
* **pandas v0.24.2** ([website](https://pandas.pydata.org)) for dataframe-based analysis.
* **root_pandas** for interfacing ROOT-TTrees to pandas dataframes([github](https://pandas.pydata.org))
* **scipy v1.2.1** ([website](https://docs.scipy.org/doc/scipy/reference/release.1.2.1.html)) for its statistics tools.
* **matplotlib v2.2.4** ([website](https://matplotlib.org)) for data visualisation.
* **tqdm** ([github](https://github.com/tqdm/tqdm)) pretty printing of loop iterations similar to a progress bar.
* **tabulate** ([website](https://pypi.org/project/tabulate/)) easy I/O of tables.

**_Other software_**

* **Texlive** ([website](https://www.tug.org/texlive/)) and its core packages for the creation (```pdflatex ...```) of the summary pdfs.
* **HexPlot** ([gitlab](https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_analysis)) for data visualisation on realistic sensor topologies.

### Configured Computer at CERN: pclcd54



### Docker Image

- A docker image with the required software installed can be found here: [thorbenquast/lcd\_hgc\_ana\_wf](https://hub.docker.com/r/thorbenquast/lcd_hgc_ana_wf).
- If one opts for this option, one must ensure that the input (raw) data are accessible within the container. Furthermore, the output files need to be exported from the container's scope to the targeted directly. Docker's ```-v <host directory>:<container directory>``` option might be helpful in this regard.
- Startup command example: ```docker run -it -v <data directory on host machine>:/home -v <output directory on host>:/output thorbenquast/lcd_hgc_ana_wf```. 

### Framework Setup

After downloading the project repository and installing the software dependencies, environmental parameters need to be set. 
The relevant environmental parameters are defined in ```setup.sh```. The following parameters need to be set appropriately on each device.





## Contributing




## Further information

