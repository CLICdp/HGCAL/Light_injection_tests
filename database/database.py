import os

#Define the names of the data base files. The main directory is stored in the DB_DIR environmental parameter.
dut_db_file = os.path.abspath(os.path.join(os.environ["DB_DIR"], "DUTs.csv"))
tct_db_file = os.path.abspath(os.path.join(os.environ["DB_DIR"], "TCTMeasurements.csv"))

from interface.DUTs import DUTs
dut_db = DUTs(dut_db_file)
from interface.tct_measurements import TCTMeasurements
tct_db = TCTMeasurements(tct_db_file)