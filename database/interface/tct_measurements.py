import pandas as pd
import numpy as np
import sys

class TCTMeasurements:
	def __init__(self, fpath):
		self.database = pd.read_csv(fpath, header=0, delimiter=",")

	def __contains__(self, key):
		return key in self.get_allIDs()

	def get_row(self, ID):
		x = self.database[self.database.ID==ID]
		if x.empty:
			sys.exit("%s not in tct database!"%ID)
		return x

	def get_allIDs(self):
		return self.database.ID.unique()

	def get_campaign(self, ID):
		return np.array(self.get_row(ID).Campaign)[0]

	def get_DUT(self, ID):
		return np.array(self.get_row(ID).DUT)[0]

	def get_date(self, ID):
		return np.array(self.get_row(ID).Date)[0]

	def get_station(self, ID):
		return np.array(self.get_row(ID).Station)[0]					

	def get_temperature(self, ID):
		return int(np.array(self.get_row(ID).Temperature)[0])	

	def get_attenuation(self, ID):
		return float(np.array(self.get_row(ID).Attenuation)[0])

	def get_oscilloscopeSetting(self, ID):
		return np.array(self.get_row(ID).OscilloscopeSetting)[0]

	def get_filedirectory(self, ID):
		return np.array(self.get_row(ID).FileDirectory)[0]
	
	def get_rawfilePattern(self, ID):
		return np.array(self.get_row(ID).RawFilePattern)[0]
