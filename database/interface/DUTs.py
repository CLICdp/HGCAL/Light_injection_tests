import pandas as pd
import numpy as np
import ast, sys

class DUTs:
	def __init__(self, fpath):
		self.database = pd.read_csv(fpath, header=0, delimiter=",")

	def get_row(self, name):
		x = self.database[self.database.Name==name]
		if x.empty:
			sys.exit("%s not in DUT database!"%name)
		return x

	def get_allDUTs(self):
		return self.database.Name.unique()

	def get_size(self, name):
		return int(np.array(self.get_row(name).Size)[0])

	def get_nchannels(self, name):
		return int(np.array(self.get_row(name).NChannels)[0])

	def get_thickness(self, name):
		return int(np.array(self.get_row(name).Thickness)[0])

	def get_doping(self, name):
		return np.array(self.get_row(name).Doping)[0]

	def get_pstop(self, name):
		return np.array(self.get_row(name).Pstop)[0]

	def get_irradiation(self, name):
		return np.array(self.get_row(name).Irradiation)[0]

	def get_geofile(self, name):
		return np.array(self.get_row(name).GeoFile)[0]