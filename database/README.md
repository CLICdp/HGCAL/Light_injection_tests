# Data Base (DB)


## User Maintainable Files

The files that need to be maintained by the user are .csv files like the ones in the [csv](csv) directory. Note that these files must contain **comma-separated** tables.

The user has the possibility to configure the location of equivalent DB files in [database.py](database.py). By default, the DB's main directory is set via the ```DB_DIR``` environmental parameter.

For the noise measurements, electronic maps are also attributed to each measurement. The default electronic maps are stored in [emaps](emaps). An alternative location of the electronic mappings may also be set via the ```EMAP_DIR``` environmental parameter.

## Programmatic Data Base Interface

The database objects can be sourced by any python program in this framework:

- DUT (device-under-test) data base: ```from database.database import dut_db```
- IV data base: ```from database.database import iv_db```
- CV data base: ```from database.database import cv_db```
- Noise data base: ```from database.database import noise_db```

The APIs of the above mentioned objects feature **read-only** methods.
Theys are defined in the following locations:

- ```dut_db``` <-- [interface/DUTs.py](interface/DUTs.py)
- ```iv_db``` <-- [interface/iv_measurements.py](interface/iv_measurements.py)
- ```cv_db``` <-- [interface/cv_measurements.py](interface/cv_measurements.py)
- ```noise_db``` <-- [interface/noise_measurements.py](interface/noise_measurements.py)
